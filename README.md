# Saltmine repo

My own repository of packages. Built to my liking.

## Packages
- dwm-saltmine [https://gitlab.com/saltmine81/dwm-saltmine.git](https://gitlab.com/saltmine81/dwm-saltmine.git)
- dmenu-saltmine [https://gitlab.com/saltmine81/dmenu-saltmine](https://gitlab.com/saltmine81/dmenu-saltmine)
- st-saltmine [https://gitlab.com/saltmine81/st-saltmine](https://gitlab.com/saltmine81/st-saltmine)

## Instruction
Add following to `/etc/pacman.conf` to enable the repo:

```
[saltmine-repo]
SigLevel = Optional DatabaseOptional
Server = https://gitlab.com/saltmine81/$repo/-/raw/master/$arch
```


To update packages in repo:

```
cd saltmine-repo/x86_64
repo-add saltmine-repo.db.tar.gz *.pkg.tar.zst
rm saltmine-repo.db saltmine-repo.files
mv saltmine-repo.db.tar.gz saltmine-repo.db
mv saltmine-repo.files.tar.gz saltmine-repo.files
```

## Credits
Thanks to Distrotube for inspiration and help getting this to work. [https://gitlab.com/dwt1/dt-arch-repo](https://gitlab.com/dwt1/dt-arch-repo)
